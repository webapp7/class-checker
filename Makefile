.PHONY: all
all: build test deploy

.PHONY: build
build:
	echo build

.PHONY: test
test:
	npm install
	# 本来はlintをかけているが，ソースコードをデモ用に調整する関係でコメントアウト
	# npx eslint *.js public/js/*.js

.PHONY: deploy
deploy:
	npm install
	sudo rsync -av ./ /home/ubuntu/classroom-map/
