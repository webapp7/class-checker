module.exports = (function() {
    "use strict";

    const { MAXDatabase } = require("./database");

    async function getClassInformation(classroom, prog, section) {
        /*
         * この関数でデモページ用のダミーデータを作って送信する
         */
        if (classroom !== "A307" || prog !== 1 || section !== 1) {
            return [];
        }
        return [
            {
                pos: "A307-I9",
                userName: "明治 一郎",
                level: 0
            },
            {
                pos: "A307-C4",
                userName: "明治 次郎",
                level: 1
            },
            {
                pos: "A307-B1",
                userName: "明治 三郎",
                level: 2
            },
            {
                pos: "A307-F8",
                userName: "明治 四郎",
                level: 3
            },
            {
                pos: "A307-H5",
                userName: "明治 五郎",
                level: 4
            },
            {
                pos: "A307-I3",
                userName: "明治 六郎",
                level: 5
            }
        ];
        /*
         * 以下，本来のソースコード
         * プログラム実習の課題提出用Webアプリのデータベースを叩いて進捗を収集する
         * const connection = new MAXDatabase();
         * connection.open();
         * const users = await connection.getUserData(classroom);
         * const problems = await connection.getProblemList(prog, section);
         * for (let user of users) {
         *     const results = await connection.getSubmissionResult(user.userId);
         *     user.level = getClearLevel(user, problems, results);
         * }
         * connection.close();
         * return users;
         */
    }

    function getClearLevel(user, problems, results) {
        let clearLevel = 0;
        for (let level of problems) {
            if (level.length === 0) {
                continue;
            }
            let flag = false;
            for (let problem of level) {
                if (!(problem.problemId in results) || !results[problem.problemId]) {
                    flag = true;
                    break;
                }
            }
            if (flag) {
                break;
            }
            clearLevel++;
        }
        return clearLevel;
    }

    return {
        getClassInformation
    }
})();
